package main.java;

import java.util.Comparator;
import java.util.Objects;

public class Employee implements PrintTriConsumer<String, String, Integer>, Comparable<Employee> {
    private static int LAST_ID = 12500; //  LAST_ID is being shared by all the instances of Employee class.
    private int id;
    private String firstName;
    private String lastName;
    private int age;

    public Employee(String firstName, String lastName, int age) {
        this.id = ++LAST_ID;    //  Pre-incrementing the value of LAST_ID and then assigning it to the id variable.
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }

    //  Copy constructor.
    public Employee(Employee e) {
        this.id = e.id;
        this.firstName = e.firstName;
        this.lastName = e.lastName;
        this.age = e.age;
    }

    @Override
    public void accept(String firstName, String lastName, Integer age) {
        System.out.println(firstName + "\t\t" + lastName + "\t\t" + age);
    }

    @Override
    public int compareTo(Employee e) {
        return this.getFirstName().compareToIgnoreCase(e.getFirstName());
    }

    static class EmployeeComparator implements Comparator<Employee> {
        private final String sortType;

        public EmployeeComparator() {
            this("firstName");
        }

        public EmployeeComparator(String sortType) {
            this.sortType = sortType;
        }

        @Override
        public int compare(Employee e1, Employee e2) {
            if (sortType.equalsIgnoreCase("id"))
                return Integer.compare(e1.getId(), e2.getId());
            if (sortType.equalsIgnoreCase("lastName"))
                return e1.getLastName().compareToIgnoreCase(e2.getLastName());
            if (sortType.equalsIgnoreCase("age"))
                return Integer.compare(e1.getAge(), e2.getAge());
            return e1.getFirstName().compareToIgnoreCase(e2.getFirstName());
        }
    }

    public static int compareIds(Employee e1, Employee e2) {
        return Integer.compare(e1.getId(), e2.getId());
    }

    public static int compareFirstNames(Employee e1, Employee e2) {
        return e1.getFirstName().compareToIgnoreCase(e2.getFirstName());
    }

    public static int compareLastNames(Employee e1, Employee e2) {
        return e1.getLastName().compareToIgnoreCase(e2.getLastName());
    }

    public static int compareAges(Employee e1, Employee e2) {
        return Integer.compare(e1.getAge(), e2.getAge());
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return "%-10s \t %-10s \t %-10s \t %d".formatted(getId(), getFirstName(), getLastName(), getAge());
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!(obj instanceof Employee e))
            return false;

        return Objects.equals(this.id, e.id) &&
                Objects.equals(this.firstName, e.firstName) &&
                Objects.equals(this.lastName, e.lastName) &&
                Objects.equals(this.age, e.age);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstName, lastName, age);
    }
}
